# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  w_lengths = {}

  str.split.each { |word| w_lengths[word] = word.length }

  w_lengths

end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.each { |k,v| return k if hash[k] == hash.values.max }
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letters = Hash.new(0)
  word.chars.each do |letter|
    letters[letter] += 1
  end
  letters
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash_count = Hash.new(0)
  arr.each do |el|
    hash_count[el] += 1
  end
  hash_count.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  counts = {even: 0, odd: 0}
  numbers.each do |num|
    if num.odd?
      counts[:odd] += 1
    else
      counts[:even] += 1
    end
  end
  counts
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  counts = Hash.new(0)
  string.chars.each do |letter|
    counts[letter] += 1 if vowels.include?(letter)
  end

  most = counts.values.max
  ties = counts.select { |k, v| v if v == most }
  ties.keys.min
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  later = students.select { |k, v| v > 6 }
  later.keys.combination(2).to_a
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  uniq = specimens.uniq
  species_count = {}

  uniq.each do |animal|
    species_count[animal] = specimens.count(animal)
  end
  number_of_spec = uniq.length
  smallest_pop = species_count.values.min
  largest_pop = species_count.values.max

  number_of_spec ** 2 * smallest_pop / largest_pop

end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal = character_count(normal_sign)
  vandalized = character_count(vandalized_sign)

  vandalized.all? do |letter, count|
    normal[letter.downcase] >= count
  end

end

def character_count(str)
  counts = Hash.new(0)
  str.chars.each { |ch| counts[ch] = str.count(ch)}
  counts
end
